package no.as.gold.simplemessenger;

import no.as.gold.simplemessenger.messages.IMessage;

/**
 * Created by aage on 06.12.13.
 */
public abstract class MessageHandler <T extends IMessage> {
    public abstract void handle(T msg);
}
