package no.as.gold.simplemessenger.messages;

/**
 * This is the interface of messages that can be sent by the {@link no.as.gold.simplemessenger.MessengerService}
 * Created by Aage Dahl on 06.12.13.
 */
public interface IMessage {
    public String getMessage();
}
