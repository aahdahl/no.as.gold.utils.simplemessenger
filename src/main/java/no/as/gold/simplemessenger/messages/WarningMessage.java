package no.as.gold.simplemessenger.messages;

/**
 * Created by Aage Dahl on 23.02.14.
 */
public class WarningMessage implements IMessage {
    private String mMsg;
    public WarningMessage(String msg) {
        mMsg = msg;
    }

    @Override
    public String getMessage() {
        return mMsg;
    }
}
