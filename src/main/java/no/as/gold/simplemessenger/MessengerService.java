package no.as.gold.simplemessenger;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import no.as.gold.simplemessenger.messages.IMessage;

/**
 * This class is designed for message passing between the fragments and activities
 * Note: An object can only register one handle for each message type (Need to be improved...)
 * @author aage
 *
 */
public class MessengerService {

    /**
     * Default messenger service, to be used in the general case
     */
    public static MessengerService Default = new MessengerService();
    private ConcurrentHashMap<Object, ConcurrentHashMap<Class<?>, ArrayList<MessageHandler>>> mHandlers = new ConcurrentHashMap<>();

    /**
     * Registers interest for a certain type of messages
     * @param key:            Identification of the handler
     * @param messageHandler: Callback function when receiving a message of the given class type
     * @param messageClass:	  Type of message to listen for
     */
    public synchronized void Register(Object key,
                                      Class<?> messageClass, MessageHandler messageHandler) {

        // Initiate new hashmap for new recepient
        if(!mHandlers.containsKey(key)) {
            ConcurrentHashMap<Class<?>, ArrayList<MessageHandler>> hashMap = new ConcurrentHashMap<>();
            mHandlers.put(key, hashMap);
        }

        ConcurrentHashMap<Class<?>, ArrayList<MessageHandler>> hashMap = mHandlers.get(key);
        if(!hashMap.containsKey(messageClass)) {
            ArrayList<MessageHandler> handlers = new ArrayList<>();
            hashMap.put(messageClass, handlers);
        }

        mHandlers.get(key).get(messageClass).add(messageHandler);
    }

    /**
     * Sends message to all callback functions that registered interest for this message type
     * @param msg: message to be transmitted
     */
    public synchronized <T extends IMessage> void send(T msg) {
        for(Object receiver : mHandlers.keySet()) {
            ConcurrentHashMap<Class<?>, ArrayList<MessageHandler>> recepientHandlers = mHandlers.get(receiver);
            // If this receiver does not have any handlers - go to next receiver
            if(recepientHandlers == null)
                continue;

            for(Class<?> key : recepientHandlers.keySet())
                if(key.isInstance(msg))
                    for(MessageHandler handler : recepientHandlers.get(key))
                        handler.handle(msg);
        }
    }

    /**
     * Unregisters the MessageHandler associated with the given receiver and IMessage
     * @param receiver Recepient with handle for the given message type
     * @param messageClass Message type to remove listener for
     */
    public synchronized void UnRegister(Object receiver,
                                        Class<?> messageClass) {
        // Identify handlers associated with receiver
        if(mHandlers.containsKey(receiver)) {
            ConcurrentHashMap<Class<?>, ArrayList<MessageHandler>> recepientHandlers = mHandlers.get(receiver);
            // Remove the handle associated with this message type
            recepientHandlers.remove(messageClass);
            // If no more message handlers, remove this receiver!
            if(recepientHandlers.isEmpty()) {
                mHandlers.remove(receiver);
            }
        }
    }

    /**
     * Unregisters all handlers
     */
    public synchronized void UnRegisterAll() {
        mHandlers.clear();
    }

    /**
     * Unregisters all listeners associated with a given receiver
     *
     * @param receiver message receiver
     */
    public void UnRegisterAll(Object receiver) {
        mHandlers.remove(receiver);
    }
}

